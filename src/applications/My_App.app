<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <label>My App</label>
    <tabs>Drinks_List__c</tabs>
    <tabs>Drinks_Order__c</tabs>
    <tabs>Customer1__c</tabs>
    <tabs>standard-report</tabs>
    <tabs>SessionName__c</tabs>
    <tabs>Speaker__c</tabs>
    <tabs>Suggestion__c</tabs>
    <tabs>Airtel__c</tabs>
    <tabs>IDEA__c</tabs>
    <tabs>Expense__c</tabs>
    <tabs>Tower__c</tabs>
    <tabs>Energy_Audit__c</tabs>
</CustomApplication>
